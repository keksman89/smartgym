package developer.ink1804.smartgym

import android.app.Application
import android.content.SharedPreferences
import developer.ink1804.smartgym.di.component.ApplicationComponent
import developer.ink1804.smartgym.di.component.DaggerApplicationComponent
import developer.ink1804.smartgym.di.module.ContextModule
import developer.ink1804.smartgym.di.module.SharedPreferencesModule
import developer.ink1804.smartgym.network.api.ApiService
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.ui.MainActivity
import timber.log.Timber

class AppApplication : Application() {

    var trainingId: Long = 0

    var trainingList: MutableList<ProgramDTO>? = null

    companion object {
        lateinit var instance: AppApplication
    }

    lateinit var currentActivity: MainActivity

    lateinit var applicationComponent: ApplicationComponent

    val apiManager: ApiService
        get() = applicationComponent.getApiModule()

    val sharedPreferences: SharedPreferences
        get() = applicationComponent.getSharedPreferences()


    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
            .contextModule(ContextModule(this))
            .sharedPreferencesModule(SharedPreferencesModule())
            .build()

        super.onCreate()

        instance = this

        Timber.plant(Timber.DebugTree())
    }
}