package developer.ink1804.smartgym.di

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext


@Qualifier
annotation class ActivityContext
