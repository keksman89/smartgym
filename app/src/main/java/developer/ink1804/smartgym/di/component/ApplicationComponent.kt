package developer.ink1804.smartgym.di.component

import android.content.SharedPreferences
import dagger.Component
import developer.ink1804.smartgym.ui.MainActivity
import developer.ink1804.smartgym.di.module.NavigationModule
import developer.ink1804.smartgym.ui.fragment.common.TabContainerFragment
import developer.ink1804.smartgym.di.module.LocalNavigationModule
import developer.ink1804.smartgym.di.module.ApiModule
import developer.ink1804.smartgym.di.module.SharedPreferencesModule
import developer.ink1804.smartgym.network.api.ApiService
import developer.ink1804.smartgym.ui.fragment.home.HomeFragment
import developer.ink1804.smartgym.ui.fragment.training.CurrentTrainerFragment
import developer.ink1804.smartgym.ui.fragment.training.MaxTrainingFragment
import developer.ink1804.smartgym.ui.fragment.training_result.TrainingResultFragment
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApiModule::class,
        SharedPreferencesModule::class,
        NavigationModule::class,
        LocalNavigationModule::class
    ]
)
interface ApplicationComponent {

    fun getApiModule(): ApiService
    fun getSharedPreferences(): SharedPreferences

    //activity
    fun injectActivity(activity: MainActivity)

    fun inject(fragment: HomeFragment)
    fun inject(fragment: MaxTrainingFragment)
    fun inject(fragment: CurrentTrainerFragment)
    fun inject(fragment: TrainingResultFragment)


    //bottom navigation
    fun inject(fragment: TabContainerFragment)


}
