package developer.ink1804.smartgym.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import developer.ink1804.smartgym.network.api.ApiInterface
import developer.ink1804.smartgym.network.api.ApiManager
import developer.ink1804.smartgym.network.api.ApiService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module(includes = [OkHttpClientModule::class])
class ApiModule {

    private val url = "http://100.100.157.145:8087/"

    @Provides
    fun provideApiService(apiInterface: ApiInterface): ApiService {
        return ApiManager(apiInterface)
//        return ApiMock()
    }

    @Singleton
    @Provides
    fun api(retrofit: Retrofit): ApiInterface {
        return retrofit.create(ApiInterface::class.java)
    }

    @Singleton
    @Provides
    fun retrofit(okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(url)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    fun gson(): Gson {
        val gsonBuilder = GsonBuilder().setDateFormat("")
        return gsonBuilder.create()
    }


    @Provides
    fun gsonConvertFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

}
