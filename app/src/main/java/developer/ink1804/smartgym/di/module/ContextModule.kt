package developer.ink1804.smartgym.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import developer.ink1804.smartgym.di.ApplicationContext
import javax.inject.Singleton

@Module
class ContextModule(private val context: Context) {

    @Singleton
    @ApplicationContext
    @Provides
    fun context(): Context {
        return context.applicationContext
    }
}
