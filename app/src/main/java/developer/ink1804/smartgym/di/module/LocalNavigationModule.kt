package developer.ink1804.smartgym.di.module

import dagger.Module
import dagger.Provides
import developer.ink1804.smartgym.navigation.LocalCiceroneHolder
import javax.inject.Singleton

@Module
class LocalNavigationModule {

    @Provides
    @Singleton
    fun provideLocalNavigationModule(): LocalCiceroneHolder {
        return LocalCiceroneHolder()
    }
}