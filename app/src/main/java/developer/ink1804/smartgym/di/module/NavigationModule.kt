package developer.ink1804.smartgym.di.module

import dagger.Module
import dagger.Provides
import developer.ink1804.smartgym.navigation.AppRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder

@Module
class NavigationModule {

    private var cicerone: Cicerone<AppRouter> = Cicerone.create(AppRouter())

    @Provides
    fun provideRouter(): AppRouter {
        return cicerone.router
    }

    @Provides
    fun provideNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }
}