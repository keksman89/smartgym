package developer.ink1804.smartgym.di.module

import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.ConnectionPool
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


@Module(includes = [ContextModule::class])
class OkHttpClientModule {

    private val readTimeout: Long = 30
    private val connectTimeout: Long = 30
    private val connectionPool = ConnectionPool(0, 1, TimeUnit.MICROSECONDS)

    @Provides
    fun okHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient().newBuilder()
            .readTimeout(readTimeout, TimeUnit.SECONDS)
            .connectTimeout(connectTimeout, TimeUnit.SECONDS)
            .connectionPool(connectionPool)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

//    @Provides
//    fun unsafeHttpClient(
////            cache: Cache,
//            httpLoggingInterceptor: HttpLoggingInterceptor
////                         @Named("queryInterceptor") queryInterceptor: Interceptor,
////                         @Named("responseInterceptor") responseInterceptor: Interceptor
//    ): OkHttpClient {
//        try {
//            // Create a trust manager that does not validate certificate chains
//            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
//                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {}
//
//                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {}
//
//                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
//                    return arrayOf()
//                }
//            })
//
//            // Install the all-trusting trust manager
//            val sslContext = SSLContext.getInstance("SSL")
//            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
//
//            // Create an ssl socket factory with our all-trusting manager
//            val sslSocketFactory = sslContext.socketFactory
//
//            val builder = OkHttpClient.Builder()
//            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
//            builder.hostnameVerifier { _, _ -> true }
//
//            return builder
////                    .cache(cache)
//                    .readTimeout(readTimeout, TimeUnit.SECONDS)
//                    .connectTimeout(connectTimeout, TimeUnit.SECONDS)
//                    .connectionPool(connectionPool)
////                    .addInterceptor(queryInterceptor)
////                    .addInterceptor(responseInterceptor)
//                    .addInterceptor(httpLoggingInterceptor)
//                    .build()
//        } catch (e: Exception) {
//            throw RuntimeException(e)
//        }
//    }

//    @Provides
//    fun cache(cacheFile: File): Cache {
//        return Cache(cacheFile, (10 * 1000 * 1000).toLong())
//    }

//    @Singleton
//    @Provides
//    fun file(@ApplicationContext context: Context): File {
//        val file = File(context.cacheDir, "HttpCache")
//        file.mkdir()
//        return file
//    }

    @Provides
    fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor { message -> Timber.d(message) }

        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return httpLoggingInterceptor
    }

//    @Provides
//    @Named("queryInterceptor")
//    fun queryInterceptor(): Interceptor {
//        return Interceptor {
//            var request = it.request()
//
//            var url = request.url()
//            url = url.newBuilder().addQueryParameter("app", "ndb24-android-test").build()
//            request = request.newBuilder().url(url).build()
//            it.proceed(request)
//        }
//    }

//    @Provides
//    @Named("responseInterceptor")
//    fun responseInterceptor(): Interceptor {
//        return Interceptor {
//            var request = it.request()
//            var response = it.proceed(request)
//            val responseString = response.peekBody(Long.MAX_VALUE)
//
//            var gson = GsonBuilder().create()
//            var a = gson.fromJson<BaseResponseModel>(responseString.string(), BaseResponseModel::class.java)
//
//            if (!a.isSuccess) {
//                Timber.wtf(a.error.id.toString())
//                    if (a.error.id == 4) {
////                        AppApplication.instance.currentActivity.route.newRootFragment(AuthActivity())
//                    }
//            }
//
//            response
//        }
//    }
}
