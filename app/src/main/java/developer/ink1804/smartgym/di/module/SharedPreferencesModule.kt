package developer.ink1804.smartgym.di.module

import android.content.Context
import dagger.Module
import android.content.SharedPreferences
import dagger.Provides
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.di.ApplicationContext
import javax.inject.Singleton


@Module(includes = [ContextModule::class])
class SharedPreferencesModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(context.getString(R.string.app_shared_pref), Context.MODE_PRIVATE)
    }
}