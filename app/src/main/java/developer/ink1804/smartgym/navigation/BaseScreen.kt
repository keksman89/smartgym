package developer.ink1804.smartgym.navigation

import android.support.v4.app.Fragment
import developer.ink1804.smartgym.ui.BaseMvpFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class BaseScreen(private var fragment: BaseMvpFragment? = null) : SupportAppScreen() {

    override fun getFragment(): Fragment {
        return if (fragment != null) fragment!! else super.getFragment()
    }
}