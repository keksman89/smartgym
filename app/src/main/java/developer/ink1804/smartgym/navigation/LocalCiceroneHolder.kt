package developer.ink1804.smartgym.navigation

import ru.terrakok.cicerone.Cicerone


class LocalCiceroneHolder {

    private var containers: MutableMap<String, Cicerone<AppRouter>> = mutableMapOf()

    fun getCicerone(containerTag: String): Cicerone<AppRouter> {
        if (!containers.containsKey(containerTag)) {
            containers[containerTag] = Cicerone.create(AppRouter())
        }

        return containers[containerTag]!!
    }
}