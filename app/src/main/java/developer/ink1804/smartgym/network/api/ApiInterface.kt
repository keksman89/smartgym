package developer.ink1804.smartgym.network.api

import developer.ink1804.smartgym.network.pojo.AuthRequest
import developer.ink1804.smartgym.network.pojo.AuthResponse
import developer.ink1804.smartgym.network.pojo.TrainingResultDTO
import developer.ink1804.smartgym.network.pojo.dto.*
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*


interface ApiInterface {

    //    @Header("Content-Type : application/json")
    @POST("emulator/user_info")
    fun postUserData(
        @Body data: UserData
    ): Observable<ResponseBody>

    @POST("emulator/trainer_result/{userId}")
    fun postTrainerResult(
        @Path("userId") userId: Long,
        @Body data: ProgramDTO
    ): Observable<ResponseBody>

    @GET("ui/connect/{userId}")
    fun connect(
        @Path("userId") userId: Long
    ): Observable<SessionToken>

    @GET("ui/disconnect/{trainingId}")
    fun disconnect(
        @Path("trainingId") trainingId: Long
    ): Observable<ResponseBody>


    @POST("ui/personal_maximum")
    fun postPersonalMax(
        @Body query: TrainerMaxDTO
    ): Observable<ResponseBody>

    @POST("ui/auth")
    fun authorize(
        @Body authorize: AuthRequest
    ): Observable<AuthResponse>

    @GET("ui/program/{userId}")
    fun getTodayProgram(
        @Path("userId") userId: Long
    ): Observable<List<ProgramDTO>>


    @GET("ui/trainer")
    fun getAllTrainers(): Observable<List<TrainerDTO>>


    @GET("ui/{trainingId}")
    fun getFinalTrainingResult(
        @Path("trainingId") id: Long
    ): Observable<List<TrainingResultDTO>>
}