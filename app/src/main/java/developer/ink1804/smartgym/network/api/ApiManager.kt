package developer.ink1804.smartgym.network.api

import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.pojo.AuthRequest
import developer.ink1804.smartgym.network.pojo.AuthResponse
import developer.ink1804.smartgym.network.pojo.dto.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import javax.inject.Inject

class ApiManager @Inject constructor(private var apiInterface: ApiInterface) : ApiService {

    override fun getTrainingResult(trainingId: Long): Observable<List<ProgramDTO>> {
        return Observable.just(AppApplication.instance.trainingList)
    }

    override fun postTrainerResult(userId: Long, trainer: ProgramDTO): Observable<ResponseBody> {
        return apiInterface.postTrainerResult(userId, trainer)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun connect(userId: Long): Observable<SessionToken> {
        return apiInterface.connect(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun disconnect(trainingId: Long): Observable<ResponseBody> {
        return apiInterface.disconnect(trainingId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun authorize(request: AuthRequest): Observable<AuthResponse> {
        return apiInterface.authorize(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun postUserData(data: UserData): Observable<ResponseBody> {
        return apiInterface.postUserData(data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getAllTrainers(): Observable<List<TrainerDTO>> {
        return apiInterface.getAllTrainers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun postMaxPower(max: Int, trainerId: Long): Observable<ResponseBody> {
        return apiInterface.postPersonalMax(TrainerMaxDTO(max, trainerId, 1))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getProgram(userId: Long): Observable<List<ProgramDTO>> {
        return apiInterface.getTodayProgram(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

