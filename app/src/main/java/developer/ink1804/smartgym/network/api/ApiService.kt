package developer.ink1804.smartgym.network.api

import developer.ink1804.smartgym.network.pojo.AuthRequest
import developer.ink1804.smartgym.network.pojo.AuthResponse
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.network.pojo.dto.SessionToken
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.network.pojo.dto.UserData
import io.reactivex.Observable
import okhttp3.ResponseBody

interface ApiService {

    fun getTrainingResult(trainingId: Long): Observable<List<ProgramDTO>>

    fun connect(userId: Long): Observable<SessionToken>

    fun disconnect(trainingId: Long): Observable<ResponseBody>

    fun authorize(request: AuthRequest): Observable<AuthResponse>

    fun postUserData(data: UserData): Observable<ResponseBody>

    fun getAllTrainers(): Observable<List<TrainerDTO>>

    fun postMaxPower(max: Int, trainerId: Long): Observable<ResponseBody>

    fun getProgram(userId: Long): Observable<List<ProgramDTO>>

    fun postTrainerResult(userId: Long, trainer: ProgramDTO): Observable<ResponseBody>


}