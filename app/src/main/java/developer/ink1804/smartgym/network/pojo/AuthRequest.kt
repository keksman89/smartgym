package developer.ink1804.smartgym.network.pojo

import com.google.gson.annotations.SerializedName

data class AuthRequest(
    @SerializedName("login")
    var login:String,
    @SerializedName("password")
    var password: String
) {

}