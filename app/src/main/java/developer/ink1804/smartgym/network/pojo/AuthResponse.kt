package developer.ink1804.smartgym.network.pojo

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("id")
    var id: Long
) {
}