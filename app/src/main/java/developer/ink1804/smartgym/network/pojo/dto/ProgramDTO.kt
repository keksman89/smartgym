package developer.ink1804.smartgym.network.pojo.dto

import com.google.gson.annotations.SerializedName


open class ProgramDTO() {

    @SerializedName("pulse")
    var pulse = listOf<Int>()
    @SerializedName("planRepeats")
    var planRepeats: Int = 0
    @SerializedName("realRepeats")
    var realRepeats: Int = 0
    @SerializedName("trainerDto")
    var trainerDTO: TrainerDTO? = null

    @SerializedName("time")
    var time: Int = 0

    @SerializedName("trainingId")
    var trainingId: Long = 0
}

