package developer.ink1804.smartgym.network.pojo.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SessionToken(
    @SerializedName("trainingId")
    var trainingId: Long,
    @SerializedName("userId")
    var userId: Long
) : Serializable