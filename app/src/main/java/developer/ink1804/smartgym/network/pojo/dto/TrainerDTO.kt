package developer.ink1804.smartgym.network.pojo.dto

import com.google.gson.annotations.SerializedName

data class TrainerDTO(
    @SerializedName("cordX")
    var coordX: Int,
    @SerializedName("cordY")
    var coordY: Int,
    @SerializedName("description")
    var description: String,
    @SerializedName("id")
    var id: Long,
    @SerializedName("mainPhotoUrl")
    var mainPhotoURL: String,
    @SerializedName("name")
    var name: String

) {}