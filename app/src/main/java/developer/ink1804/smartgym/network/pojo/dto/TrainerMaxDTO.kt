package developer.ink1804.smartgym.network.pojo.dto

import com.google.gson.annotations.SerializedName

data class TrainerMaxDTO(
    @SerializedName("maxValue")
    var maxValue: Int,
    @SerializedName("trainerId")
    var trainerId: Long,
    @SerializedName("userId")
    var userId: Long
)