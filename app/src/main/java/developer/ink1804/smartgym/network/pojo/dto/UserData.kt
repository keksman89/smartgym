package developer.ink1804.smartgym.network.pojo.dto

import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("userId")
    var userId: Int,

    @SerializedName("weight")
    var weight: Int,

    @SerializedName("height")
    var height: Int,

    @SerializedName("fatPercent")
    var fatPercent: Int,

    @SerializedName("musclePercent")
    var musclePercent: Int
)