package developer.ink1804.smartgym.presentation.presenter.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.api.ApiService
import developer.ink1804.smartgym.network.pojo.AuthRequest
import developer.ink1804.smartgym.presentation.presenter.common.BaseMvpPresenter
import developer.ink1804.smartgym.presentation.view.auth.AuthView
import developer.ink1804.smartgym.utils.saveUserId
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class AuthPresenter @Inject constructor() : BaseMvpPresenter<AuthView>() {

    @Inject
    lateinit var apiService: ApiService

    fun onAuthClick(login: String, password: String){
//        api.post(){ viewState.performAuth() }

        viewState.showProgress()
        var d = apiService.authorize(AuthRequest(login, password))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                saveUserId(it.id)
                viewState.hideProgress()
                viewState.performAuth()
            },{
                viewState.hideProgress()
                viewState.showMessage(it.message.toString())
            })

        disposeBag.add(d)
    }
}
