package developer.ink1804.smartgym.presentation.presenter.common

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable

open class BaseMvpPresenter<View : MvpView> : MvpPresenter<View>() {

    protected var disposeBag = CompositeDisposable()

    //TODO: onStart(){ disposeBag.init() }

    override fun onDestroy() {
        super.onDestroy()

        disposeBag.dispose()
    }
}