package developer.ink1804.smartgym.presentation.presenter.common

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.presentation.view.common.TabContainerView
import javax.inject.Inject

@InjectViewState
class TabContainerPresenter @Inject constructor() : MvpPresenter<TabContainerView>() {

}
