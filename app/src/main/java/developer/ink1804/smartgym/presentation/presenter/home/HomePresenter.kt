package developer.ink1804.smartgym.presentation.presenter.home

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.network.api.ApiManager
import developer.ink1804.smartgym.presentation.view.home.HomeView
import javax.inject.Inject

@InjectViewState
class HomePresenter @Inject constructor() : MvpPresenter<HomeView>() {

    @Inject
    lateinit var apiManager: ApiManager
}
