package developer.ink1804.smartgym.presentation.presenter.main

import developer.ink1804.smartgym.presentation.presenter.common.BaseMvpPresenter
import developer.ink1804.smartgym.presentation.view.main.MainView
import javax.inject.Inject

class MainPresenter @Inject constructor() : BaseMvpPresenter<MainView>() {

}