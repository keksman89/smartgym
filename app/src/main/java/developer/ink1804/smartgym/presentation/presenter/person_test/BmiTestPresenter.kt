package developer.ink1804.smartgym.presentation.presenter.person_test

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.presentation.view.person_test.BmiTestView

@InjectViewState
class BmiTestPresenter : MvpPresenter<BmiTestView>() {

}
