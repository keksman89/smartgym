package developer.ink1804.smartgym.presentation.presenter.profile

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.presentation.view.profile.ProfileView
import javax.inject.Inject

@InjectViewState
class ProfilePresenter @Inject constructor() : MvpPresenter<ProfileView>() {

}
