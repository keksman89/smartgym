package developer.ink1804.smartgym.presentation.presenter.splash

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.presentation.view.splash.SplashView
import javax.inject.Inject

@InjectViewState
class SplashPresenter @Inject constructor() : MvpPresenter<SplashView>() {

}
