package developer.ink1804.smartgym.presentation.presenter.training

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.network.api.ApiManager
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.presentation.view.training.CurrentTrainerView
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class CurrentTrainerPresenter @Inject constructor() : MvpPresenter<CurrentTrainerView>() {

    @Inject
    lateinit var apiManager: ApiManager

    fun getProgram(userId: Long) {
        var d = apiManager.getProgram(userId).subscribe({
            viewState.initRecyclerView(it)
        }, {
            Timber.wtf(it.toString())
        })
    }

    fun postResult(userId: Long, trainer: ProgramDTO) {
        var d = apiManager.postTrainerResult(userId, trainer).subscribe({
            viewState.finishTraining()
        }, {
            Timber.wtf(it.toString())
        })

    }
}
