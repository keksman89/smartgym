package developer.ink1804.smartgym.presentation.presenter.training

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.network.api.ApiManager
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.presentation.view.training.MaxTrainingView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@InjectViewState
class MaxTrainingPresenter @Inject constructor() : MvpPresenter<MaxTrainingView>() {

    @Inject
    lateinit var apiManager: ApiManager

    fun getAllTrainers() {
        var d = apiManager.getAllTrainers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.initRecyclerView(it)
            }, {
                Timber.wtf(it.toString())
            })
    }

    fun postMaxResult(trainer: TrainerDTO, max: Int) {
        var d = apiManager.postMaxPower(Random().nextInt(40) + 10, trainer.id)
            .subscribe({

            }, {
                Timber.wtf(it.toString())
            })
    }
}
