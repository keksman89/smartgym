package developer.ink1804.smartgym.presentation.presenter.training_result

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.smartgym.network.api.ApiManager
import developer.ink1804.smartgym.presentation.view.training_result.TrainingResultView
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class TrainingResultPresenter @Inject constructor() : MvpPresenter<TrainingResultView>() {

    @Inject
    lateinit var apiManager: ApiManager

    fun getResult(trainingId: Long) {
        var d = apiManager.getTrainingResult(trainingId)
            .subscribe({
                viewState.showData(it)
            }, {
                Timber.wtf(it.toString())
            })
    }

}
