package developer.ink1804.smartgym.presentation.view.auth

import com.arellomobile.mvp.MvpView
import developer.ink1804.smartgym.presentation.view.common.BaseMvpActivityView

interface AuthView : BaseMvpActivityView {

    fun performAuth()
}
