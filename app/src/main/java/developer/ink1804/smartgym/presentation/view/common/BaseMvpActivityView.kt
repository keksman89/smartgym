package developer.ink1804.smartgym.presentation.view.common

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView

interface BaseMvpActivityView : MvpView {

    fun showMessage(message: String)
    fun showMessage(@StringRes messageId: Int)

    fun showProgress()
    fun hideProgress()
}