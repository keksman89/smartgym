package developer.ink1804.smartgym.presentation.view.common

import android.support.annotation.StringRes

interface BaseMvpFragmentView{

    fun showProgress()
    fun hideProgress()

    fun showMessage(message: String)
    fun showMessage(@StringRes messageId: Int)
}