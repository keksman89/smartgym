package developer.ink1804.smartgym.presentation.view.training

import com.arellomobile.mvp.MvpView
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO

interface CurrentTrainerView : MvpView {
    fun initRecyclerView(items: List<ProgramDTO>)

    fun postTrainerData(trainer: ProgramDTO)

    fun finishTraining()
}
