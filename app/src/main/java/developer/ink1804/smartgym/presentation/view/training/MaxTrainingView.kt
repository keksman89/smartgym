package developer.ink1804.smartgym.presentation.view.training

import com.arellomobile.mvp.MvpView
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO

interface MaxTrainingView : MvpView {

    fun initRecyclerView(items: List<TrainerDTO>)

    fun postTrainerData(trainer: TrainerDTO)
}
