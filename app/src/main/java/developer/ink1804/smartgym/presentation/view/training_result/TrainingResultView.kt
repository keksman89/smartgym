package developer.ink1804.smartgym.presentation.view.training_result

import com.arellomobile.mvp.MvpView
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO

interface TrainingResultView : MvpView {

    fun showData(list: List<ProgramDTO>)
}
