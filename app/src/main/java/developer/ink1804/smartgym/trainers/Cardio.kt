package developer.ink1804.smartgym.trainers

interface Cardio : Trainer {
    fun startTimer()
    fun stopTimer()
}