package developer.ink1804.smartgym.trainers

interface Force : Trainer {
    fun startTimer()
    fun stopTimer()
}