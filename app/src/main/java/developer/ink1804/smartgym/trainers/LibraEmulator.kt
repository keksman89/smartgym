package developer.ink1804.smartgym.trainers

import android.os.Handler
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.api.ApiManager
import developer.ink1804.smartgym.network.pojo.dto.UserData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*


class LibraEmulator(var apiManager: ApiManager) : Trainer {

    var weight: Int = 0
    var height: Int = 0

    var approximateWeight: Int = Random().nextInt(80) + 40


    var delay = 50.toLong()
    var ticks = 0
    lateinit var handler: Handler

    var listener: LibraListener? = null

    override fun startTraining() {
        handler = Handler()

        height = Random().nextInt(40) + 150

        handler.postDelayed({ tick() }, delay)
    }

    fun tick() {
        weight = Random().nextInt(5) + approximateWeight

        listener?.postValue(weight)
        ticks++

        if (ticks < 10) {
            handler.postDelayed({ tick() }, delay)
            delay += 20
        } else {
            handler.postDelayed({ finishTraining() }, 1000)
        }
    }

    override fun finishTraining() {
        Timber.v(UserData(1, weight, height, getFarPercent(), getMusculPercent()).toString())
        var d = apiManager.postUserData(
            UserData(1, weight, height, getFarPercent(), getMusculPercent())
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Timber.v(it.string().toString())
                listener?.onFinish()
            }, {
                Timber.v(it.toString())
            })

        var d1 = apiManager.connect(1).subscribe({
            AppApplication.instance.trainingId = it.trainingId
        }, {
            Timber.wtf(it.toString())
        })
    }

    override fun saveApproach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getFarPercent(): Int {
        return (0.31457 * (height - weight) - 0.10969 * weight + 10.834).toInt()
    }

    fun getMusculPercent(): Int {
        return weight - (weight * (getFarPercent() / 100)) - 30
    }

}

interface LibraListener {
    fun postValue(value: Int)
    fun onFinish()
}