package developer.ink1804.smartgym.trainers

interface Trainer {
    fun startTraining()
    fun finishTraining()
    fun saveApproach()
}