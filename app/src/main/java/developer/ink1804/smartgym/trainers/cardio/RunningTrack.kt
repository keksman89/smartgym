package developer.ink1804.smartgym.trainers.cardio

import developer.ink1804.smartgym.trainers.Cardio
import developer.ink1804.smartgym.trainers.common.TimerForce

class RunningTrack(var count: Int) : Cardio {

    private lateinit var timerForce: TimerForce
    private var time = "0"

    override fun startTimer() {
        timerForce = TimerForce(count, 5)
        timerForce.run()
    }

    fun pauseTimer() {
        timerForce.onPause()
    }


    override fun stopTimer() {
        timerForce.onStop()
    }

    override fun startTraining() {
        startTimer()
    }

    override fun finishTraining() {
        stopTimer()
    }

    override fun saveApproach() {
        time = timerForce.timer
    }
}