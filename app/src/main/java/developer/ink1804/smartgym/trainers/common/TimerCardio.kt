package developer.ink1804.smartgym.trainers.common

import android.os.Handler
import android.util.Log

class TimerCardio(var needTime: Int) : Runnable {
    private var handlerflag: Boolean = true
    private lateinit var handler: Handler
    private var second = 0
    var time = "0"

    override fun run() {
        handler = Handler()
        handler.post {
            val hours = second / 3600
            val minutes = second % 3600 / 60
            val secs = second % 60

            time = String.format(
                "%d:%02d:%02d",
                hours, minutes, secs
            )
            if (handlerflag) {
                second++
                if (second == needTime)
                    stop()
                Log.w("TAG", "RUN TIMER: $second")
                handler.postDelayed(this, 1000)
            }
        }
    }

    fun pause() {
        handlerflag = false
    }

    fun stop() {
        second = 0
        handlerflag = false
    }
}