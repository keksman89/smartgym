package developer.ink1804.smartgym.trainers.common

import android.os.Handler
import android.util.Log

class TimerForce(var count: Int, var time: Int) : Runnable {
    private var handlerflag: Boolean = true
    private lateinit var handler: Handler
    private var second = 0
    var timer = "0"

    override fun run() {
        handler = Handler()
        handler.post {
            val hours = second / 3600
            val minutes = second % 3600 / 60
            val secs = second % 60

            timer = String.format(
                "%d:%02d:%02d",
                hours, minutes, secs
            )
            if (handlerflag) {
                second++
                Log.w("TAG", "RUN TIMER: $second")
                if (second == count)
                    onStop()
                handler.postDelayed(this, time.toLong())
            }
        }
    }

    fun onPause() {
        handlerflag = false
    }

    fun onStop() {
        second = 0
        handlerflag = false
    }
}