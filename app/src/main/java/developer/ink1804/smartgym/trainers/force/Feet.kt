package developer.ink1804.smartgym.trainers.force

import developer.ink1804.smartgym.trainers.Force
import developer.ink1804.smartgym.trainers.common.TimerForce

class Feet(var count: Int) : Force {
    private lateinit var timerForce: TimerForce
    private var time = "0"

    override fun startTimer() {
        timerForce = TimerForce(count, 1000)
        timerForce.run()
    }

    override fun stopTimer() {
        timerForce.onStop()
    }

    override fun startTraining() {
        startTimer()
    }

    override fun finishTraining() {
        stopTimer()
    }

    override fun saveApproach() {
        time = timerForce.timer
    }

}