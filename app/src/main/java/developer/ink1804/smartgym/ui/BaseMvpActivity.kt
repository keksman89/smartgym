package developer.ink1804.smartgym.ui

import android.app.Dialog
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatActivity
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.navigation.AppRouter
import developer.ink1804.smartgym.presentation.view.common.BaseMvpActivityView
import developer.ink1804.smartgym.ui.common.RouterProvider
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import javax.inject.Inject

abstract class BaseMvpActivity : MvpAppCompatActivity(), BaseMvpActivityView, RouterProvider {

    @Inject
    lateinit var navigationHolder: NavigatorHolder

    @Inject
    lateinit var activityRouter: AppRouter

    fun getActivity(): MvpAppCompatActivity {
        return this
    }

    override fun getRouter(): AppRouter {
        return activityRouter
    }

    private var navigator = object : SupportAppNavigator(getActivity(), R.id.activityFragmentContainer) {
        override fun applyCommands(commands: Array<Command>) {
            super.applyCommands(commands)
            supportFragmentManager.executePendingTransactions()
        }
    }

    lateinit var disposeBag: CompositeDisposable
    private var dialog: Dialog? = null

    private lateinit var coordinatorLayout: CoordinatorLayout

    private fun createDialog() {
        if (dialog == null) {
            val builder = AlertDialog.Builder(this)
            builder.setView(R.layout.dlg_progress)
            builder.setCancelable(false)
            this.dialog = builder.create()
        }
    }

    override fun onResume() {
        super.onResume()
        navigationHolder.setNavigator(navigator)
        disposeBag = CompositeDisposable()
        coordinatorLayout = window.decorView.findViewById(R.id.coordinatorLayout)
    }

    override fun onPause() {
        super.onPause()
        navigationHolder.removeNavigator()
        disposeBag.dispose()
    }

    override fun showMessage(message: String) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showMessage(messageId: Int) {
        Snackbar.make(coordinatorLayout, getString(messageId), Snackbar.LENGTH_LONG).show()
    }

    override fun showProgress() {
        if (dialog == null) {
            createDialog()
        }

        dialog!!.show()
    }

    override fun hideProgress() {
        if (dialog != null) {
            dialog!!.dismiss()
        }
    }
}