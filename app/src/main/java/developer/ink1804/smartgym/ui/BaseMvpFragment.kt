package developer.ink1804.smartgym.ui

import android.os.Bundle
import android.support.annotation.StringRes
import android.view.View
import com.arellomobile.mvp.MvpAppCompatFragment
import developer.ink1804.smartgym.navigation.AppRouter
import developer.ink1804.smartgym.presentation.view.common.BaseMvpFragmentView
import developer.ink1804.smartgym.ui.common.BackButtonListener
import developer.ink1804.smartgym.ui.common.RouterProvider
import io.reactivex.disposables.CompositeDisposable


abstract class BaseMvpFragment : MvpAppCompatFragment(), BaseMvpFragmentView, BackButtonListener {

    var disposeBag = CompositeDisposable()

    lateinit var rootView: View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootView = view
        initViews()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun getParentRouter(): AppRouter? {
        if (parentFragment != null && parentFragment is RouterProvider) {
            return (parentFragment as RouterProvider).getRouter()
        }

        return null
    }

    open fun initViews() {

    }

    fun getRootActivity(): BaseMvpActivity {
        return activity as BaseMvpActivity
    }

    override fun showProgress() {
        getRootActivity().showProgress()
    }

    override fun hideProgress() {
        getRootActivity().hideProgress()
    }

    override fun showMessage(message: String) {
        getRootActivity().showMessage(message)
    }

    override fun showMessage(@StringRes messageId: Int) {
        getRootActivity().showMessage(getString(messageId))
    }

    override fun onPause() {
        super.onPause()

        if (!disposeBag.isDisposed)
            disposeBag.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        disposeBag = CompositeDisposable()
    }

    override fun onBackPressed(): Boolean {
        getParentRouter()?.exit()
        return true
    }
}