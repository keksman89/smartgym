package developer.ink1804.smartgym.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.WindowManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.presenter.main.MainPresenter
import developer.ink1804.smartgym.presentation.view.main.MainView
import developer.ink1804.smartgym.ui.common.BackButtonListener
import developer.ink1804.smartgym.utils.TAB_HISTORY
import developer.ink1804.smartgym.utils.TAB_HOME
import developer.ink1804.smartgym.utils.TAB_PROFILE
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.inc_toolbar.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : BaseMvpActivity(), MainView {

    @Inject
    @InjectPresenter
    lateinit var presenter: MainPresenter

    var onBackPressedListener: (() -> Boolean)? = null

    @ProvidePresenter
    fun providePresenter(): MainPresenter {
        return presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.injectActivity(this)

        val window = window
        // window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        //  window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        toolbar.title = "Главная"
        initBottomBar()
        AppApplication.instance.currentActivity = this

        if (supportFragmentManager.fragments.size == 0)
            startFragment()
    }


    fun configureActionBar(backButtonEnabled: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(backButtonEnabled)
        supportActionBar?.setDisplayShowHomeEnabled(backButtonEnabled)
    }

    private fun startFragment() {
        selectTab(TAB_HOME)
    }

    private fun initBottomBar() {
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.item_home -> {
                    selectTab(TAB_HOME)
                }
                R.id.item_profile -> {
                    selectTab(TAB_PROFILE)
                }

                R.id.item_history -> {
                    selectTab(TAB_HISTORY)

                }
            }
            true
        }
    }

    private fun selectTab(tab: String) {
        val fm = supportFragmentManager
        var currentFragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                currentFragment = f
                break
            }
        }


        val newFragment = fm.findFragmentByTag(tab)

        if (currentFragment != null && newFragment != null && currentFragment === newFragment) return

        val transaction = fm.beginTransaction()

        if (newFragment == null) {
            transaction.add(R.id.activityFragmentContainer, TabScreen(tab).fragment, tab)
        }

        if (currentFragment != null) {
            transaction.hide(currentFragment)
        }

        if (newFragment != null) {
            transaction.show(newFragment)
        }

        transaction.commit()
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }
        if (fragment != null
            && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()
        ) {
            return
        } else {
            super.onBackPressed()
        }
    }
}
