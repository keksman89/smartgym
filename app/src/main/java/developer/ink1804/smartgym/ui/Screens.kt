package developer.ink1804.smartgym.ui

import android.support.v4.app.Fragment
import developer.ink1804.smartgym.ui.fragment.common.TabContainerFragment
import developer.ink1804.smartgym.ui.fragment.home.HomeFragment
import developer.ink1804.smartgym.ui.fragment.profile.ProfileFragment
import developer.ink1804.smartgym.ui.fragment.training.CurrentTrainerFragment
import developer.ink1804.smartgym.ui.fragment.training.MaxTrainingFragment
import developer.ink1804.smartgym.ui.fragment.training_result.TrainingResultFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class TabScreen(private val tabName: String) : SupportAppScreen() {

    override fun getFragment(): Fragment {
        return TabContainerFragment.getNewInstance(tabName)
    }
}

class HomeScreen : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return HomeFragment()
    }
}

class ProfileScreen : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return ProfileFragment()
    }
}

class TrainingResultScreen : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return TrainingResultFragment()
    }
}


//class BmiTestScreen : SupportAppScreen() {
//    override fun getFragment(): Fragment {
//        return BmiTestDialogFragment()
//    }
//}
//
class MaxTrainingScreen : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return MaxTrainingFragment()
    }
}

class CurrentTrainingScreen : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return CurrentTrainerFragment()
    }
}
