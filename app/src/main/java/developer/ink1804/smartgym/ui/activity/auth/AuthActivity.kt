package developer.ink1804.smartgym.ui.activity.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle

import com.arellomobile.mvp.presenter.InjectPresenter
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.auth.AuthView
import developer.ink1804.smartgym.presentation.presenter.auth.AuthPresenter
import developer.ink1804.smartgym.ui.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_auth.*
import javax.inject.Inject

class AuthActivity : BaseMvpActivity(), AuthView {
    companion object {
        const val TAG = "AuthActivity"
        fun getIntent(context: Context): Intent = Intent(context, AuthActivity::class.java)
    }

    @Inject
    @InjectPresenter
    lateinit var mAuthPresenter: AuthPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        button_login.setOnClickListener {
            mAuthPresenter.onAuthClick(et_login.text.toString(), et_password.text.toString())
        }
    }

    override fun performAuth() {
//        startActivity()
    }
}
