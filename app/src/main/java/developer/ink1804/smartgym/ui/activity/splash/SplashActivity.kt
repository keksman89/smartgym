package developer.ink1804.smartgym.ui.activity.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.arellomobile.mvp.MvpAppCompatActivity

import com.arellomobile.mvp.presenter.InjectPresenter
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.splash.SplashView
import developer.ink1804.smartgym.presentation.presenter.splash.SplashPresenter
import developer.ink1804.smartgym.ui.BaseMvpActivity
import developer.ink1804.smartgym.ui.MainActivity
import developer.ink1804.smartgym.ui.activity.auth.AuthActivity


class SplashActivity : MvpAppCompatActivity(), SplashView {

    companion object {
        const val TAG = "SplashActivity"
        fun getIntent(context: Context): Intent = Intent(context, SplashActivity::class.java)
    }

    @InjectPresenter
    lateinit var mSplashPresenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            startActivity(intent)
            finish()
        }, 1000)
    }
}
