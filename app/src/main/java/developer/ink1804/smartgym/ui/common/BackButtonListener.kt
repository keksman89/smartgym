package developer.ink1804.smartgym.ui.common

interface BackButtonListener {
    fun onBackPressed(): Boolean
}