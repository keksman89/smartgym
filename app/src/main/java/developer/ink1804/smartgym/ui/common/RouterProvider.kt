package developer.ink1804.smartgym.ui.common

import developer.ink1804.smartgym.navigation.AppRouter

interface RouterProvider {
    fun getRouter(): AppRouter
}