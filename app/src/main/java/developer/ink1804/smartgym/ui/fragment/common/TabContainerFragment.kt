package developer.ink1804.smartgym.ui.fragment.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.R

import developer.ink1804.smartgym.navigation.AppRouter
import developer.ink1804.smartgym.ui.BaseMvpFragment
import developer.ink1804.smartgym.ui.common.BackButtonListener
import developer.ink1804.smartgym.ui.common.RouterProvider
import developer.ink1804.smartgym.navigation.LocalCiceroneHolder
import developer.ink1804.smartgym.ui.HomeScreen
import developer.ink1804.smartgym.ui.ProfileScreen
import developer.ink1804.smartgym.ui.TrainingResultScreen
import developer.ink1804.smartgym.utils.TAB_HISTORY
import developer.ink1804.smartgym.utils.TAB_HOME
import developer.ink1804.smartgym.utils.TAB_PROFILE
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import timber.log.Timber
import javax.inject.Inject

class TabContainerFragment : BaseMvpFragment(), RouterProvider, BackButtonListener {

    @Inject
    lateinit var ciceroneHolder: LocalCiceroneHolder

    private var navigator: Navigator? = null

    companion object {
        const val EXTRA_NAME = "tcf_extra_name"

        fun getNewInstance(name: String): TabContainerFragment {
            val fragment = TabContainerFragment()

            val args = Bundle()
            Timber.v("tabName: $name")
            args.putString(EXTRA_NAME, name)
            fragment.arguments = args

            return fragment
        }
    }

    private fun getContainerName(): String {
        return arguments!!.getString(EXTRA_NAME, "Nothing")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    private fun getCicerone(): Cicerone<AppRouter> {
        return ciceroneHolder.getCicerone(getContainerName())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_container, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.tabFragmentContainer) == null) {
            when (getContainerName()) {
                TAB_HOME -> getCicerone().router.replaceScreen(HomeScreen())
                TAB_PROFILE -> getCicerone().router.replaceScreen(ProfileScreen())
                TAB_HISTORY -> getCicerone().router.replaceScreen(TrainingResultScreen())
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getCicerone().navigatorHolder.setNavigator(getNavigator())
    }

    override fun onPause() {
        getCicerone().navigatorHolder.removeNavigator()
        super.onPause()
    }

    private fun getNavigator(): Navigator {
        if (navigator == null) {
            navigator = SupportAppNavigator(activity, childFragmentManager, R.id.tabFragmentContainer)
        }

        return navigator!!
    }

    override fun getRouter(): AppRouter {
        return getCicerone().router
    }

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.tabFragmentContainer)
        return if (fragment != null
                && fragment is BackButtonListener
                && (fragment as BackButtonListener).onBackPressed()) {
            true
        } else {
            (activity as RouterProvider).getRouter().exit()
            true
        }
    }
}