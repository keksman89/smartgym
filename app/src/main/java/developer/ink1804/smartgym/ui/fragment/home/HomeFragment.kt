package developer.ink1804.smartgym.ui.fragment.home

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.home.HomeView
import developer.ink1804.smartgym.presentation.presenter.home.HomePresenter

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.trainers.LibraEmulator
import developer.ink1804.smartgym.trainers.LibraListener
import developer.ink1804.smartgym.ui.BaseMvpFragment
import developer.ink1804.smartgym.ui.CurrentTrainingScreen
import developer.ink1804.smartgym.ui.MaxTrainingScreen
import developer.ink1804.smartgym.ui.fragment.person_test.BottomSheetMaxParamsFragment
import kotlinx.android.synthetic.main.fragment_bmi_test.*
import kotlinx.android.synthetic.main.fragment_bmi_test.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import javax.inject.Inject

class HomeFragment : BaseMvpFragment(), HomeView {
    companion object {
        const val TAG = "HomeFragment"

        fun newInstance(): HomeFragment {
            val fragment: HomeFragment = HomeFragment()
            val args: Bundle = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    @InjectPresenter
    lateinit var mHomePresenter: HomePresenter

    @ProvidePresenter
    fun providePresenter(): HomePresenter {
        return mHomePresenter
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rootView.btStartBmiTest.setOnClickListener {
            var alertDialog: AlertDialog? = null
            val v = LayoutInflater.from(context!!)
                .inflate(R.layout.fragment_bmi_test, rootView.container, false)

            v.btConnect.setOnClickListener {
                val emul = LibraEmulator(mHomePresenter.apiManager)
                emul.listener = object : LibraListener {
                    override fun postValue(value: Int) {
                        v.tvValue.text = "$value кг"
                    }

                    override fun onFinish() {
                        alertDialog?.dismiss()
                    }
                }
                emul.startTraining()
            }

            alertDialog = AlertDialog.Builder(context!!)
                .setView(v)
                .create()
            alertDialog.show()
        }

        rootView.btStartMaxParamsTest.setOnClickListener {
            getParentRouter()?.navigateTo(MaxTrainingScreen())
        }

        rootView.btStartTraining.setOnClickListener {
            getParentRouter()?.navigateTo(CurrentTrainingScreen())
        }

    }
}
