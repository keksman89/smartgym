package developer.ink1804.smartgym.ui.fragment.person_test

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.person_test.BmiTestView
import developer.ink1804.smartgym.presentation.presenter.person_test.BmiTestPresenter

import com.arellomobile.mvp.presenter.InjectPresenter

class BmiTestDialogFragment : DialogFragment(), BmiTestView {
    companion object {
        const val TAG = "BmiTestDialogFragment"

        fun newInstance(): BmiTestDialogFragment {
            val fragment: BmiTestDialogFragment = BmiTestDialogFragment()
            val args: Bundle = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mBmiTestPresenter: BmiTestPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bmi_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
