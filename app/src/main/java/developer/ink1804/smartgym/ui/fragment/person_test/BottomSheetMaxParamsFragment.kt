package developer.ink1804.smartgym.ui.fragment.person_test

import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.CoordinatorLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.network.api.ApiService
import kotlinx.android.synthetic.main.fragment_bottom_sheet_max_params.view.*
import javax.inject.Inject

class BottomSheetMaxParamsFragment : BottomSheetDialogFragment() {

    public var onDismiss: (() -> Unit)? = null

    @Inject
    lateinit var apiService: ApiService

    fun newInstance(productId: String): BottomSheetMaxParamsFragment {
        val args = Bundle()
        args.putString("id", productId)

        val fragment = BottomSheetMaxParamsFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.TransparentBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        scrollMax()
        return inflater.inflate(R.layout.fragment_bottom_sheet_max_params, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btApply.setOnClickListener {
            //TODO api postrequest
            dismiss()
        }
    }

    private fun scrollMax() {
        dialog.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
            val coordinatorLayout = bottomSheet!!.parent as CoordinatorLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.peekHeight = 5000
            coordinatorLayout.parent.requestLayout()
        }
    }


    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)

        onDismiss?.invoke()
    }
}

