package developer.ink1804.smartgym.ui.fragment.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.profile.ProfileView
import developer.ink1804.smartgym.presentation.presenter.profile.ProfilePresenter

import com.arellomobile.mvp.presenter.InjectPresenter
import developer.ink1804.smartgym.ui.BaseMvpFragment

class ProfileFragment : BaseMvpFragment(), ProfileView {
    companion object {

        fun newInstance(): ProfileFragment {
            val fragment: ProfileFragment = ProfileFragment()
            val args: Bundle = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mProfilePresenter: ProfilePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
