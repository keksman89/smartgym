package developer.ink1804.smartgym.ui.fragment.training

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.presentation.view.training.MaxTrainingView

class AdtMaxTraining(val list: List<TrainerDTO>, var currentTrainingView: MaxTrainingView) :
    RecyclerView.Adapter<MaxTrainerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MaxTrainerViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.current_training_item, parent, false)
        return MaxTrainerViewHolder(v, currentTrainingView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: MaxTrainerViewHolder, position: Int) {
        holder.bind(list[position])
    }


}