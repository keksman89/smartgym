package developer.ink1804.smartgym.ui.fragment.training

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.training.CurrentTrainerView
import developer.ink1804.smartgym.presentation.presenter.training.CurrentTrainerPresenter

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.ui.BaseMvpFragment
import developer.ink1804.smartgym.ui.TrainingResultScreen
import kotlinx.android.synthetic.main.fragment_current_training.view.*
import javax.inject.Inject

class CurrentTrainerFragment : BaseMvpFragment(), CurrentTrainerView {
    @Inject
    @InjectPresenter
    lateinit var mCurrentTrainingPresenter: CurrentTrainerPresenter

    @ProvidePresenter
    fun providePresenter(): CurrentTrainerPresenter {
        return mCurrentTrainingPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_current_training, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mCurrentTrainingPresenter.getProgram(1)

    }

    override fun initRecyclerView(items: List<ProgramDTO>) {
        rootView.recyclerView.layoutManager = LinearLayoutManager(context)
        rootView.recyclerView.adapter = AdtCurrentTraining(items, this)
    }

    override fun postTrainerData(trainer: ProgramDTO) {
        mCurrentTrainingPresenter.postResult(1, trainer)
    }

    override fun finishTraining() {
        getParentRouter()?.navigateTo(TrainingResultScreen())
    }
}
