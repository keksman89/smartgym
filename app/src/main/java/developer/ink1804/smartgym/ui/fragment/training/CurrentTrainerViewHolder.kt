package developer.ink1804.smartgym.ui.fragment.training

import android.os.Build
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import androidx.annotation.RequiresApi
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.presentation.view.training.CurrentTrainerView
import developer.ink1804.smartgym.presentation.view.training.MaxTrainingView
import kotlinx.android.synthetic.main.current_training_item.view.*
import java.util.*

class CurrentTrainerViewHolder(
    itemView: View,
    var currentTrainingView: CurrentTrainerView? = null
) :
    RecyclerView.ViewHolder(itemView) {

    val handler = Handler()
    var mItem: ProgramDTO? = null

    var max: Int = 0
    var current: Int = 0

    @RequiresApi(Build.VERSION_CODES.N)
    fun bind(item: ProgramDTO) {
        this.mItem = item
        max = item.planRepeats
        current = 0

        itemView.tvProgress.text = "$current/$max"
        itemView.tvName.text = item.trainerDTO!!.name
        itemView.pbProgress.max = item.planRepeats

        if (currentTrainingView == null) {
            itemView.shadowView.visibility = View.VISIBLE
        }

        itemView.btConnect.setOnClickListener {
            loadProgress()
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun loadProgress() {
        handler.postDelayed({
            current = itemView.pbProgress.progress + 1
            itemView.pbProgress.setProgress(current, true)
            itemView.tvProgress.text = "$current/$max"

            if (max == itemView.pbProgress.progress) {
                itemView.shadowView.visibility = View.VISIBLE
                mItem?.realRepeats = current
                mItem?.trainingId = AppApplication.instance.trainingId
                mItem?.time = Random().nextInt(5) + 5

                AppApplication.instance.trainingList = mutableListOf(mItem!!)
                currentTrainingView?.postTrainerData(mItem!!)
            } else {
                loadProgress()
            }
        }, 1000)
    }
}