package developer.ink1804.smartgym.ui.fragment.training

import android.os.Build
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import androidx.annotation.RequiresApi
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.presentation.view.training.MaxTrainingView
import kotlinx.android.synthetic.main.current_training_item.view.*

class MaxTrainerViewHolder(itemView: View, var currentTrainingView: MaxTrainingView) :
    RecyclerView.ViewHolder(itemView) {

    var mItem: TrainerDTO? = null
    @RequiresApi(Build.VERSION_CODES.N)
    fun bind(item: TrainerDTO) {
        this.mItem = item

        itemView.tvName.text = item.name
        itemView.pbProgress.max = 1

        itemView.btConnect.setOnClickListener {
            loadProgress(1)
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun loadProgress(maxValue: Int) {
        val handler = Handler()

        handler.postDelayed({
            itemView.pbProgress.setProgress(itemView.pbProgress.progress + 1, true)

            if (maxValue == itemView.pbProgress.progress) {
                itemView.shadowView.visibility = View.VISIBLE
                currentTrainingView.postTrainerData(mItem!!)
            }
        }, 1000)
    }
}