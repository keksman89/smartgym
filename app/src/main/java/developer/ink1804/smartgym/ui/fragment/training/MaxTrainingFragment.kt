package developer.ink1804.smartgym.ui.fragment.training

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import developer.ink1804.smartgym.R
import developer.ink1804.smartgym.presentation.view.training.MaxTrainingView
import developer.ink1804.smartgym.presentation.presenter.training.MaxTrainingPresenter

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.pojo.dto.TrainerDTO
import developer.ink1804.smartgym.ui.BaseMvpFragment
import kotlinx.android.synthetic.main.fragment_current_training.view.*
import javax.inject.Inject

class MaxTrainingFragment : BaseMvpFragment(), MaxTrainingView {

    @Inject
    @InjectPresenter
    lateinit var mCurrentTrainingPresenter: MaxTrainingPresenter

    @ProvidePresenter
    fun providePresenter(): MaxTrainingPresenter {
        return mCurrentTrainingPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_current_training, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mCurrentTrainingPresenter.getAllTrainers()

    }

    override fun initRecyclerView(items: List<TrainerDTO>) {
        rootView.recyclerView.layoutManager = LinearLayoutManager(context)
        rootView.recyclerView.adapter = AdtMaxTraining(items, this)
    }

    override fun postTrainerData(trainer: TrainerDTO) {
        mCurrentTrainingPresenter.postMaxResult(trainer, 0)
    }
}
