package developer.ink1804.smartgym.ui.fragment.training_result

import com.robinhood.spark.SparkAdapter
import kotlin.random.Random

class MySparkAdapter : SparkAdapter() {

    private val yData: FloatArray

    private val random: Random

    init {
        random = Random
        yData = FloatArray(50)
        randomize()
    }


    fun randomize() {
        var i = 0
        val count = yData.size
        while (i < count) {
            yData[i] = random.nextFloat()
            i++
        }
        notifyDataSetChanged()
    }


    override fun getCount(): Int {
        return yData.size
    }


    override fun getItem(index: Int): Any {
        return yData[index]
    }


    override fun getY(index: Int): Float {
        return yData[index]
    }

}