package developer.ink1804.smartgym.ui.fragment.training_result

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import developer.ink1804.smartgym.presentation.view.training_result.TrainingResultView
import developer.ink1804.smartgym.presentation.presenter.training_result.TrainingResultPresenter

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import developer.ink1804.smartgym.AppApplication
import developer.ink1804.smartgym.network.pojo.dto.ProgramDTO
import developer.ink1804.smartgym.ui.BaseMvpFragment
import developer.ink1804.smartgym.ui.fragment.training.AdtCurrentTraining
import kotlinx.android.synthetic.main.current_training_item.view.*
import kotlinx.android.synthetic.main.fragment_training_result.*
import kotlinx.android.synthetic.main.fragment_training_result.view.*
import javax.inject.Inject


class TrainingResultFragment : BaseMvpFragment(), TrainingResultView {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: MySparkAdapter

    @Inject
    @InjectPresenter
    lateinit var mTrainingResultPresenter: TrainingResultPresenter

    @ProvidePresenter
    fun providePresenter(): TrainingResultPresenter {
        return mTrainingResultPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            developer.ink1804.smartgym.R.layout.fragment_training_result,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mTrainingResultPresenter.getResult(AppApplication.instance.trainingId)

//        loadProgress()
    }

    override fun showData(list: List<ProgramDTO>) {
        adapter = MySparkAdapter()
        rootView.sparkview.adapter = adapter

        adapter.randomize()

        linearLayoutManager = LinearLayoutManager(context)
        rv_data.layoutManager = linearLayoutManager
        rv_data.adapter = AdtCurrentTraining(list, null)
    }
}
