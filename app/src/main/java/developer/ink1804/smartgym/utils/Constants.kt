package developer.ink1804.smartgym.utils

const val TAB_HOME = "TabHomeFragment"
const val TAB_SETTINGS = "TabSettingsFragment"
const val TAB_PROFILE = "TabProfileFragment"
const val TAB_HISTORY = "TabHistoryFragment"