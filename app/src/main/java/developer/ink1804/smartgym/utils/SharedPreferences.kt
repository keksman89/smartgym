package developer.ink1804.smartgym.utils

import developer.ink1804.smartgym.AppApplication

    fun saveUserId(id: Long){
        AppApplication.instance.sharedPreferences.edit()
            .putLong("userId", id)
            .apply()
    }
